//
//  HeroСountersViewController.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 12/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HeroCountersViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var heroImageView: UIImageView!
    @IBOutlet weak var countersCollectionView: UICollectionView!
    @IBOutlet weak var heroNameLabel: UILabel!
    
    var interstitial: GADInterstitial!
    var heroName: String = ""
    var heroDictionary: Dictionary <String, Any> = [:]
    var countersArray: Array <String> = []
    
    //MARK:- Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupUI()
        self.setupObjects()
    }

    //MARK:- Navigation
    
    //MARK:- Delegates
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.countersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroCollectionViewCell", for: indexPath as IndexPath) as! HeroCollectionViewCell
        cell.heroImageView.image = UIImage(named: self.countersArray[indexPath.row])
        cell.heroNameLabel.text = self.countersArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: UIScreen.main.bounds.size.width - heroImageView.frame.size.width - 41, height: 70)
    }
    
    //MARK:- Events
    //MARK:- Actions
    
    func setupUI()
    {
        title = self.heroName
        heroImageView.image = UIImage(named: heroName)
       // heroNameLabel.text = self.heroName
        
        if (User.adsRemoved() == false && UserDefaults.standard.bool(forKey: "firstLaunch") == false)
        {
            self.setupBanner()
        }
    }
    
    func setupObjects()
    {
        countersArray = heroDictionary["All"] as! Array
        countersCollectionView.reloadData()
    }
    
    func setupBanner()
    {
        interstitial = GADInterstitial(adUnitID: AdBannerView.fullScreenBannerID)
        let request = GADRequest()
        interstitial.load(request)
        
        self.showBanner()
    }
    
    func showBanner()
    {
        if interstitial.isReady
        {
            interstitial.present(fromRootViewController: self)
        }
        else
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1)
            {
                self.showBanner()
            }
        }
    }
}
