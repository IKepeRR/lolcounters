//
//  SettingsViewController.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 22/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit
import SwiftRater
import MessageUI

enum Settings
{
    case settingsRemoveAds
    case settingsRestorePurchases
    case settingsWriteReview
    case settingsWriteToDevelopers
}

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate
{
    @IBOutlet weak var settingsTableView: UITableView!
    
    var settingsArray: Array <Settings> = []
    
    //MARK:- Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
        setupObjects()
    }
    
    //MARK:- Navigation
    //MARK:- Delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = settingsTableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as! SettingTableViewCell
        
        cell.setupCell(setting: settingsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch settingsArray[indexPath.row]
        {
        case Settings.settingsRemoveAds:
            if (StoreManager.shared().removeAdsProduct != nil)
            {
                StoreManager.shared().buyProduct(product: StoreManager.shared().removeAdsProduct)
            }
            break
            
        case Settings.settingsRestorePurchases:
            StoreManager.shared().restorePurchases()
            break
            
        case Settings.settingsWriteReview:
            SwiftRater.rateApp(host: self)
            break
            
        case Settings.settingsWriteToDevelopers:
            sendEmail()
            break
        }
    }
    
    //MARK: - MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        if (result == MFMailComposeResult.sent)
        {
            showAlert(title: "", message:NSLocalizedString("settingsWriteSuggestionSuccess", comment: ""))
        }
        
        if (result == MFMailComposeResult.failed)
        {
            showAlert(title: NSLocalizedString("error", comment: ""), message:NSLocalizedString("tryAgainLater", comment: ""))
        }
        
        controller.dismiss(animated: true)
    }
    
    //MARK:- Events
    //MARK:- Actions
    
    func setupUI()
    {
        self.title = NSLocalizedString("settings", comment: "")
        settingsTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    func setupObjects()
    {
        settingsArray = [Settings.settingsRemoveAds, Settings.settingsRestorePurchases, Settings.settingsWriteReview, Settings.settingsWriteToDevelopers]
        
        settingsTableView.reloadData()
    }
    
    func sendEmail()
    {
        if MFMailComposeViewController.canSendMail()
        {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["immmyst@gmail.com"])
            mail.setSubject("LoL Counters. Write review or suggestion")
            present(mail, animated: true)
        }
        else
        {
            showAlert(title: NSLocalizedString("error", comment: ""), message:NSLocalizedString("settingsWriteSuggestionError", comment: ""))
        }
    }
    
    func showAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
