//
//  SettingTableViewCell.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 23/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell
{
    @IBOutlet weak var settingNameLabel: UILabel!
    @IBOutlet weak var settingOptionLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(setting: Settings)
    {
        switch setting
        {
            case Settings.settingsRemoveAds:
                settingNameLabel.text = NSLocalizedString("settingsRemoveAds", comment: "")
                settingOptionLabel.text = User.adsRemoved() == false ? String(format: "%@", StoreManager.shared().removeAdsProduct?.localizedPrice ?? ""):""
                break
            case Settings.settingsRestorePurchases:
                settingNameLabel.text = NSLocalizedString("settingsRestorePurchases", comment: "")
                settingOptionLabel.text = ""
                break
            case Settings.settingsWriteReview:
                settingNameLabel.text = NSLocalizedString("settingsWriteReview", comment: "")
                settingOptionLabel.text = ""
                break
            case Settings.settingsWriteToDevelopers:
                settingNameLabel.text = NSLocalizedString("settingsWriteSuggestion", comment: "")
                settingOptionLabel.text = ""
                break
        }
    }
}
