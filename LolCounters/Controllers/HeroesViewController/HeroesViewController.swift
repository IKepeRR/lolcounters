//
//  HeroesViewController.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 11/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SnapKit
import SwiftRater
import Reachability

class HeroesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate
{
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var heroesCollectionView: UICollectionView!
    var heroesArray: Array <String> = []
    var filteredHeroesArray: Array <String> = []
    var heroesDictionary: Dictionary<String, Any> = [:]
    
    //MARK:- Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupObjects()
        setupUI()
    }
    
    //MARK:- Navigation
    
    //MARK:- Delegates
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return filteredHeroesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroCollectionViewCell", for: indexPath as IndexPath) as! HeroCollectionViewCell
        cell.heroImageView.image = UIImage(named: filteredHeroesArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (UIScreen.main.bounds.size.width - 16)/7, height: (UIScreen.main.bounds.size.width - 16)/7)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let reachability = Reachability()!
        
        if (reachability.connection == .none && User.adsRemoved() == false)
        {
            showAlert(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("internetError", comment: ""))
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller = storyboard.instantiateViewController(withIdentifier: "HeroCountersViewController") as! HeroCountersViewController
            viewcontroller.heroDictionary =  heroesDictionary[filteredHeroesArray[indexPath.row]] as! Dictionary
            viewcontroller.heroName = filteredHeroesArray[indexPath.row]
            navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if (searchText.count > 0)
        {
            filteredHeroesArray = heroesArray.filter{$0.localizedCaseInsensitiveContains(searchText)}
            searchBar.showsCancelButton = true
        }
        else
        {
            filteredHeroesArray = heroesArray
        }
        
        heroesCollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        
        filteredHeroesArray = heroesArray
        heroesCollectionView.reloadData()
        
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
    //MARK:- Events
    
    //MARK:- Actions
    
    func setupUI()
    {
        self.title = NSLocalizedString("heroes", comment: "")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.searchBar.isTranslucent = false
        self.searchBar.tintColor = .white
        
        SwiftRater.check()
    }
    
    func setupObjects()
    {
        if let path = Bundle.main.path(forResource: "Counters", ofType: "plist")
        {
            if let dictionary = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject>
            {
                heroesDictionary = dictionary
                heroesArray = Array(heroesDictionary.keys).sorted()
                filteredHeroesArray = heroesArray
                
                heroesCollectionView.reloadData()
            }
        }
    }
    
    func showAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
