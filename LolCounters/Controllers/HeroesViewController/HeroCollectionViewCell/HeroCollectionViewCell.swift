//
//  HeroCollectionViewCell.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 11/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit

class HeroCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var heroImageView: UIImageView!
    @IBOutlet weak var heroNameLabel: UILabel!
}
