//
//  AdBannerView.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 22/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SnapKit

class AdBannerView: UIView
{
    static let bannerID = "ca-app-pub-3940256099942544/2934735716"
    static let fullScreenBannerID = "ca-app-pub-3940256099942544/4411468910"
    
    @IBOutlet weak var rootViewController: UIViewController!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(removeAds), name: NSNotification.Name(rawValue: "RemoveAdsNotification"), object: nil)
        
        if (User.adsRemoved())
        {
            removeAds()
        }
        else
        {
            setupBanner()
        }
    }
    
    func setupBanner()
    {
        let bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        self.addSubview(bannerView)
        bannerView.adUnitID = AdBannerView.bannerID
        bannerView.rootViewController = rootViewController;
        bannerView.load(GADRequest())
        
        bannerView.snp.makeConstraints{
            (make) -> Void in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    @objc func removeAds()
    {
        heightConstraint.constant = 0
        self.isHidden = true
    }
}
