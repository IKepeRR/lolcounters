//
//  User.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 26/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit

#if DEBUG
    let isDebug = true
#else
    let isDebug = false
#endif

class User
{
    static func adsRemoved() -> Bool
    {
        return (UserDefaults.standard.bool(forKey: "adsRemoved") == true || isDebug == true)
    }
}
