//
//  StoreManager.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 22/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import StoreKit

let removeAdsPurchase = "aaa.lolcounters.removeAds"

class StoreManager
{
    var removeAdsProduct: SKProduct!
    
    // MARK: - Properties
    private static var sharedStoreManager: StoreManager =
    {
        let storeManager = StoreManager()
        return storeManager
    }()
    
    class func shared() -> StoreManager
    {
        return sharedStoreManager
    }
    
    //MARK: - Actions
    
    func checkCompletedTransactions()
    {
        SwiftyStoreKit.completeTransactions(atomically: true)
        { purchases in
            for purchase in purchases
            {
                switch purchase.transaction.transactionState
                {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction
                    {
                        self.getPurchase(id:purchase.productId)
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                case .failed, .purchasing, .deferred:
                break
                    
                @unknown default:
                    break
                }
            }
        }
    }
    
    func getPurchase(id: String)
    {
        if (id == removeAdsPurchase)
        {
            UserDefaults.standard.set(true, forKey: "adsRemoved")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveAdsNotification"), object: nil)
        }
    }
    
    func getProducts()
    {
        SwiftyStoreKit.retrieveProductsInfo([removeAdsPurchase])
        { result in
            if let product = result.retrievedProducts.first
            {
                self.removeAdsProduct = product
            }
        }
    }
    
    func buyProduct(product: SKProduct)
    {
        SwiftyStoreKit.purchaseProduct(product, quantity: 1, atomically: true)
        { result in
            switch result
            {
             case .success(let purchase):
             self.getPurchase(id: purchase.productId)
                break
                
            case .error(let error):
                NSLog(error.localizedDescription)
                break
            }
            
        }
    }
    
    func restorePurchases()
    {
        SwiftyStoreKit.restorePurchases(atomically: true)
        { results in
            if results.restoredPurchases.count > 0
            {
                results.restoredPurchases.forEach
                { purchase in
                  self.getPurchase(id: purchase.productId)
                }
            }
        }
    }
}
