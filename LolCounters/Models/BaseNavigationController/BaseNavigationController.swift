//
//  BaseNavigationController.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 23/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

class BaseNavigationController: UINavigationController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        self.navigationBar.barStyle = .default
        self.navigationBar.barTintColor = UIColor("#512f9d")
        self.navigationBar.shadowImage = UIImage.init(named: "barShadow")
        self.navigationBar.tintColor = .white
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .black), .foregroundColor: UIColor.white]
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
}
