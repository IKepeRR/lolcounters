//
//  BaseTabBarController.swift
//  LolCounters
//
//  Created by Ashot Asoyan on 23/06/2019.
//  Copyright © 2019 Ashot Asoyan. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tabBar.isTranslucent = false
        self.tabBar.barTintColor = UIColor("#19191C")
        
        self.tabBar.items?[0].title = NSLocalizedString("heroes", comment: "")
        self.tabBar.items?[1].title = NSLocalizedString("settings", comment: "")
        
        self.tabBar.items?[0].image = UIImage(named: "tabBarIcon1")
        self.tabBar.items?[0].selectedImage = UIImage(named: "tabBarIcon1Selected")
        
        self.tabBar.items?[1].image = UIImage(named: "tabBarIcon2")
        self.tabBar.items?[1].selectedImage = UIImage(named: "tabBarIcon2Selected")
        
        let appearance = UITabBarItem.appearance(whenContainedInInstancesOf: [BaseTabBarController.self])
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor("#5d5d5d"), NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .bold)], for: .normal)
        appearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor("#8944f0"), NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .bold)], for: .selected)
    }
}
